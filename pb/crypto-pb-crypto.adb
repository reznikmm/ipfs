with Ada.Unchecked_Deallocation;
with PB_Support.IO;
with PB_Support.Internal;

package body Crypto.Pb.Crypto is

   type Integer_Key_Type is  range 0 .. 3
     with Size => Pb.Crypto.Key_Type'Size;

   package Key_Type_IO is
     new PB_Support.IO.Enum_IO
       (Pb.Crypto.Key_Type, Integer_Key_Type, Pb.Crypto.Key_Type_Vectors);

   function Length (Self : Public_Key_Vector) return Natural is
   begin
      return Self.Length;
   end Length;

   procedure Clear (Self : in out Public_Key_Vector) is
   begin
      Self.Length := 0;
   end Clear;

   procedure Free is new Ada.Unchecked_Deallocation
     (Public_Key_Array, Public_Key_Array_Access);

   procedure Append (Self : in out Public_Key_Vector; V    : Public_Key) is
      Init_Length : constant Positive :=
        Positive'Max (1, 256 / Public_Key'Size);
   begin
      if Self.Length = 0 then
         Self.Data :=  new Public_Key_Array (1 .. Init_Length);

      elsif Self.Length = Self.Data'Last then
         Self.Data :=
           new Public_Key_Array'
             (Self.Data.all & Public_Key_Array'(1 .. Self.Length => <>));
      end if;
      Self.Length := Self.Length + 1;
      Self.Data (Self.Length) := V;
   end Append;

   overriding procedure Adjust (Self : in out Public_Key_Vector) is
   begin
      if Self.Length > 0 then
         Self.Data := new Public_Key_Array'(Self.Data (1 .. Self.Length));
      end if;
   end Adjust;

   overriding procedure Finalize (Self : in out Public_Key_Vector) is
   begin
      if Self.Data /= null then
         Free (Self.Data);
      end if;
   end Finalize;

   not overriding function Get_Public_Key_Variable_Reference
    (Self  : aliased in out Public_Key_Vector;
     Index : Positive)
      return Public_Key_Variable_Reference is
   begin
      return (Element => Self.Data (Index)'Access);
   end Get_Public_Key_Variable_Reference;

   not overriding function Get_Public_Key_Constant_Reference
    (Self  : aliased Public_Key_Vector;
     Index : Positive)
      return Public_Key_Constant_Reference is
   begin
      return (Element => Self.Data (Index)'Access);
   end Get_Public_Key_Constant_Reference;

   procedure Read_Public_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : out Public_Key) is
      Key : aliased PB_Support.IO.Key;
   begin
      while PB_Support.IO.Read_Key (Stream, Key'Access) loop
         case Key.Field is
            when 1 =>
               Key_Type_IO.Read (Stream, Key.Encoding, V.PB_Type);
            when 2 =>
               PB_Support.IO.Read (Stream, Key.Encoding, V.Data);
            when others =>
               PB_Support.IO.Unknown_Field (Stream, Key.Encoding);
         end case;
      end loop;
   end Read_Public_Key;

   procedure Write_Public_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : Public_Key) is
   begin
      if Stream.all not in PB_Support.Internal.Stream then
         declare
            WS : aliased PB_Support.Internal.Stream (Stream);
         begin
            Write_Public_Key (WS'Access, V);
            return;
         end;
      end if;
      declare
         WS : PB_Support.Internal.Stream renames
           PB_Support.Internal.Stream (Stream.all);
      begin
         WS.Start_Message;
         Key_Type_IO.Write (WS, 1, V.PB_Type);
         WS.Write (2, V.Data);
         if WS.End_Message then
            Write_Public_Key (WS'Access, V);
         end if;
      end;
   end Write_Public_Key;

   function Length (Self : Private_Key_Vector) return Natural is
   begin
      return Self.Length;
   end Length;

   procedure Clear (Self : in out Private_Key_Vector) is
   begin
      Self.Length := 0;
   end Clear;

   procedure Free is new Ada.Unchecked_Deallocation
     (Private_Key_Array, Private_Key_Array_Access);

   procedure Append (Self : in out Private_Key_Vector; V    : Private_Key) is
      Init_Length : constant Positive :=
        Positive'Max (1, 256 / Private_Key'Size);
   begin
      if Self.Length = 0 then
         Self.Data :=  new Private_Key_Array (1 .. Init_Length);

      elsif Self.Length = Self.Data'Last then
         Self.Data :=
           new Private_Key_Array'
             (Self.Data.all & Private_Key_Array'(1 .. Self.Length => <>));
      end if;
      Self.Length := Self.Length + 1;
      Self.Data (Self.Length) := V;
   end Append;

   overriding procedure Adjust (Self : in out Private_Key_Vector) is
   begin
      if Self.Length > 0 then
         Self.Data := new Private_Key_Array'(Self.Data (1 .. Self.Length));
      end if;
   end Adjust;

   overriding procedure Finalize (Self : in out Private_Key_Vector) is
   begin
      if Self.Data /= null then
         Free (Self.Data);
      end if;
   end Finalize;

   not overriding function Get_Private_Key_Variable_Reference
    (Self  : aliased in out Private_Key_Vector;
     Index : Positive)
      return Private_Key_Variable_Reference is
   begin
      return (Element => Self.Data (Index)'Access);
   end Get_Private_Key_Variable_Reference;

   not overriding function Get_Private_Key_Constant_Reference
    (Self  : aliased Private_Key_Vector;
     Index : Positive)
      return Private_Key_Constant_Reference is
   begin
      return (Element => Self.Data (Index)'Access);
   end Get_Private_Key_Constant_Reference;

   procedure Read_Private_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : out Private_Key) is
      Key : aliased PB_Support.IO.Key;
   begin
      while PB_Support.IO.Read_Key (Stream, Key'Access) loop
         case Key.Field is
            when 1 =>
               Key_Type_IO.Read (Stream, Key.Encoding, V.PB_Type);
            when 2 =>
               PB_Support.IO.Read (Stream, Key.Encoding, V.Data);
            when others =>
               PB_Support.IO.Unknown_Field (Stream, Key.Encoding);
         end case;
      end loop;
   end Read_Private_Key;

   procedure Write_Private_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : Private_Key) is
   begin
      if Stream.all not in PB_Support.Internal.Stream then
         declare
            WS : aliased PB_Support.Internal.Stream (Stream);
         begin
            Write_Private_Key (WS'Access, V);
            return;
         end;
      end if;
      declare
         WS : PB_Support.Internal.Stream renames
           PB_Support.Internal.Stream (Stream.all);
      begin
         WS.Start_Message;
         Key_Type_IO.Write (WS, 1, V.PB_Type);
         WS.Write (2, V.Data);
         if WS.End_Message then
            Write_Private_Key (WS'Access, V);
         end if;
      end;
   end Write_Private_Key;

end Crypto.Pb.Crypto;