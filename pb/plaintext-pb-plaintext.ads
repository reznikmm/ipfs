with Ada.Finalization;
with Ada.Streams;
with Crypto.Pb.Crypto;
with PB_Support.Stream_Element_Vector_Vectors;

package Plaintext.Pb.Plaintext is

   type Exchange_Vector is tagged private
     with Variable_Indexing => Get_Exchange_Variable_Reference,
     Constant_Indexing => Get_Exchange_Constant_Reference;

   type Exchange is
     record
        Id     : PB_Support.Stream_Element_Vector_Vectors.Option;
        Pubkey : Crypto.Pb.Crypto.Optional_Public_Key;
     end record;

   type Optional_Exchange  (Is_Set : Boolean := False) is
     record
        case Is_Set is
           when True =>
              Value : Pb.Plaintext.Exchange;
           when False =>
              null;
        end case;
     end record;

   function Length (Self : Exchange_Vector) return Natural;

   procedure Clear (Self : in out Exchange_Vector);

   procedure Append (Self : in out Exchange_Vector; V    : Exchange);

   type Exchange_Variable_Reference  (Element : not null access Exchange) is
     null record
     with Implicit_Dereference => Element;

   not overriding function Get_Exchange_Variable_Reference
    (Self  : aliased in out Exchange_Vector;
     Index : Positive)
      return Exchange_Variable_Reference
     with Inline;

   type Exchange_Constant_Reference
     (Element : not null access constant Exchange) is null record
     with Implicit_Dereference => Element;

   not overriding function Get_Exchange_Constant_Reference
    (Self  : aliased Exchange_Vector;
     Index : Positive)
      return Exchange_Constant_Reference
     with Inline;
private

   procedure Read_Exchange
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : out Exchange);

   procedure Write_Exchange
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : Exchange);

   for Exchange'Read use Read_Exchange;

   for Exchange'Write use Write_Exchange;

   type Exchange_Array is array (Positive range <>) of aliased Exchange;

   type Exchange_Array_Access is access Exchange_Array;

   type Exchange_Vector is
     new Ada.Finalization.Controlled
     with record
        Data   : Exchange_Array_Access;
        Length : Natural := 0;
     end record;

   overriding procedure Adjust (Self : in out Exchange_Vector);

   overriding procedure Finalize (Self : in out Exchange_Vector);

end Plaintext.Pb.Plaintext;