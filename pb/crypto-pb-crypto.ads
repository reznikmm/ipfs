with Ada.Finalization;
with Ada.Streams;
with League.Stream_Element_Vectors;
with PB_Support.Vectors;

package Crypto.Pb.Crypto is

   type Key_Type is (RSA, Ed25519, Secp256k1, ECDSA);

   for Key_Type use
     (RSA       => 0, Ed25519   => 1, Secp256k1 => 2, ECDSA     => 3);

   package Key_Type_Vectors is new PB_Support.Vectors (Key_Type);

   type Public_Key_Vector is tagged private
     with Variable_Indexing => Get_Public_Key_Variable_Reference,
     Constant_Indexing => Get_Public_Key_Constant_Reference;

   type Private_Key_Vector is tagged private
     with Variable_Indexing => Get_Private_Key_Variable_Reference,
     Constant_Indexing => Get_Private_Key_Constant_Reference;

   type Public_Key is
     record
        PB_Type : Pb.Crypto.Key_Type := Pb.Crypto.RSA;
        Data    : League.Stream_Element_Vectors.Stream_Element_Vector;
     end record;

   type Optional_Public_Key  (Is_Set : Boolean := False) is
     record
        case Is_Set is
           when True =>
              Value : Pb.Crypto.Public_Key;
           when False =>
              null;
        end case;
     end record;

   function Length (Self : Public_Key_Vector) return Natural;

   procedure Clear (Self : in out Public_Key_Vector);

   procedure Append (Self : in out Public_Key_Vector; V    : Public_Key);

   type Public_Key_Variable_Reference
     (Element : not null access Public_Key) is null record
     with Implicit_Dereference => Element;

   not overriding function Get_Public_Key_Variable_Reference
    (Self  : aliased in out Public_Key_Vector;
     Index : Positive)
      return Public_Key_Variable_Reference
     with Inline;

   type Public_Key_Constant_Reference
     (Element : not null access constant Public_Key) is null record
     with Implicit_Dereference => Element;

   not overriding function Get_Public_Key_Constant_Reference
    (Self  : aliased Public_Key_Vector;
     Index : Positive)
      return Public_Key_Constant_Reference
     with Inline;

   type Private_Key is
     record
        PB_Type : Pb.Crypto.Key_Type := Pb.Crypto.RSA;
        Data    : League.Stream_Element_Vectors.Stream_Element_Vector;
     end record;

   type Optional_Private_Key  (Is_Set : Boolean := False) is
     record
        case Is_Set is
           when True =>
              Value : Pb.Crypto.Private_Key;
           when False =>
              null;
        end case;
     end record;

   function Length (Self : Private_Key_Vector) return Natural;

   procedure Clear (Self : in out Private_Key_Vector);

   procedure Append (Self : in out Private_Key_Vector; V    : Private_Key);

   type Private_Key_Variable_Reference
     (Element : not null access Private_Key) is null record
     with Implicit_Dereference => Element;

   not overriding function Get_Private_Key_Variable_Reference
    (Self  : aliased in out Private_Key_Vector;
     Index : Positive)
      return Private_Key_Variable_Reference
     with Inline;

   type Private_Key_Constant_Reference
     (Element : not null access constant Private_Key) is null record
     with Implicit_Dereference => Element;

   not overriding function Get_Private_Key_Constant_Reference
    (Self  : aliased Private_Key_Vector;
     Index : Positive)
      return Private_Key_Constant_Reference
     with Inline;
private

   procedure Read_Public_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : out Public_Key);

   procedure Write_Public_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : Public_Key);

   for Public_Key'Read use Read_Public_Key;

   for Public_Key'Write use Write_Public_Key;

   type Public_Key_Array is array (Positive range <>) of aliased Public_Key;

   type Public_Key_Array_Access is access Public_Key_Array;

   type Public_Key_Vector is
     new Ada.Finalization.Controlled
     with record
        Data   : Public_Key_Array_Access;
        Length : Natural := 0;
     end record;

   overriding procedure Adjust (Self : in out Public_Key_Vector);

   overriding procedure Finalize (Self : in out Public_Key_Vector);

   procedure Read_Private_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : out Private_Key);

   procedure Write_Private_Key
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : Private_Key);

   for Private_Key'Read use Read_Private_Key;

   for Private_Key'Write use Write_Private_Key;

   type Private_Key_Array is array (Positive range <>) of aliased Private_Key;

   type Private_Key_Array_Access is access Private_Key_Array;

   type Private_Key_Vector is
     new Ada.Finalization.Controlled
     with record
        Data   : Private_Key_Array_Access;
        Length : Natural := 0;
     end record;

   overriding procedure Adjust (Self : in out Private_Key_Vector);

   overriding procedure Finalize (Self : in out Private_Key_Vector);

end Crypto.Pb.Crypto;