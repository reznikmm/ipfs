with Ada.Unchecked_Deallocation;
with PB_Support.IO;
with PB_Support.Internal;

package body Plaintext.Pb.Plaintext is

   package Public_Key_IO is
     new PB_Support.IO.Message_IO
       (Crypto.Pb.Crypto.Public_Key, Crypto.Pb.Crypto.Public_Key_Vector,
        Crypto.Pb.Crypto.Append);

   function Length (Self : Exchange_Vector) return Natural is
   begin
      return Self.Length;
   end Length;

   procedure Clear (Self : in out Exchange_Vector) is
   begin
      Self.Length := 0;
   end Clear;

   procedure Free is new Ada.Unchecked_Deallocation
     (Exchange_Array, Exchange_Array_Access);

   procedure Append (Self : in out Exchange_Vector; V    : Exchange) is
      Init_Length : constant Positive := Positive'Max (1, 256 / Exchange'Size);
   begin
      if Self.Length = 0 then
         Self.Data :=  new Exchange_Array (1 .. Init_Length);

      elsif Self.Length = Self.Data'Last then
         Self.Data :=
           new Exchange_Array'
             (Self.Data.all & Exchange_Array'(1 .. Self.Length => <>));
      end if;
      Self.Length := Self.Length + 1;
      Self.Data (Self.Length) := V;
   end Append;

   overriding procedure Adjust (Self : in out Exchange_Vector) is
   begin
      if Self.Length > 0 then
         Self.Data := new Exchange_Array'(Self.Data (1 .. Self.Length));
      end if;
   end Adjust;

   overriding procedure Finalize (Self : in out Exchange_Vector) is
   begin
      if Self.Data /= null then
         Free (Self.Data);
      end if;
   end Finalize;

   not overriding function Get_Exchange_Variable_Reference
    (Self  : aliased in out Exchange_Vector;
     Index : Positive)
      return Exchange_Variable_Reference is
   begin
      return (Element => Self.Data (Index)'Access);
   end Get_Exchange_Variable_Reference;

   not overriding function Get_Exchange_Constant_Reference
    (Self  : aliased Exchange_Vector;
     Index : Positive)
      return Exchange_Constant_Reference is
   begin
      return (Element => Self.Data (Index)'Access);
   end Get_Exchange_Constant_Reference;

   procedure Read_Exchange
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : out Exchange) is
      Key : aliased PB_Support.IO.Key;
   begin
      while PB_Support.IO.Read_Key (Stream, Key'Access) loop
         case Key.Field is
            when 1 =>
               if  not V.Id.Is_Set then
                  V.Id := (True, others => <>);
               end if;
               PB_Support.IO.Read (Stream, Key.Encoding, V.Id.Value);
            when 2 =>
               if  not V.Pubkey.Is_Set then
                  V.Pubkey := (True, others => <>);
               end if;
               Public_Key_IO.Read (Stream, Key.Encoding, V.Pubkey.Value);
            when others =>
               PB_Support.IO.Unknown_Field (Stream, Key.Encoding);
         end case;
      end loop;
   end Read_Exchange;

   procedure Write_Exchange
    (Stream : access Ada.Streams.Root_Stream_Type'Class;
     V      : Exchange) is
   begin
      if Stream.all not in PB_Support.Internal.Stream then
         declare
            WS : aliased PB_Support.Internal.Stream (Stream);
         begin
            Write_Exchange (WS'Access, V);
            return;
         end;
      end if;
      declare
         WS : PB_Support.Internal.Stream renames
           PB_Support.Internal.Stream (Stream.all);
      begin
         WS.Start_Message;
         if V.Id.Is_Set then
            WS.Write (1, V.Id.Value);
         end if;
         if V.Pubkey.Is_Set then
            WS.Write_Key ((2, PB_Support.Length_Delimited));
            Crypto.Pb.Crypto.Public_Key'Write (Stream, V.Pubkey.Value);
         end if;
         if WS.End_Message then
            Write_Exchange (WS'Access, V);
         end if;
      end;
   end Write_Exchange;

end Plaintext.Pb.Plaintext;