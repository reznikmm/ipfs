# IPFS in Ada

This is a playgroubd repository for an Ada IPFS stack implementation.

## Install

### Build from sources

Unpack source and run `make`.

### Dependencies

It depends on
 * [Network](https://github.com/reznikmm/network) library
 * [Protobuf](https://github.com/reznikmm/protobuf)

## Usage

TBD

## Maintainer

[Max Reznik](https://github.com/reznikmm).

