with Ada.Streams;
with League.Strings;

with Network.Addresses;
with Network.Connection_Promises;
with Network.Connections;

with P2P.Crypto;
with P2P.Peers;

package P2P.Connections is

   type Connection is tagged limited private;
   --  Conn is a connection to a remote peer. It multiplexes streams. Usually
   --  there is no need to use a Conn directly, but it may be useful to get
   --  information about the peer on the other side:
   --    stream.Conn().RemotePeer()

   -- ConnSecurity --

   not overriding function Local_Peer
     (Self : Connection) return P2P.Peers.Id;
   --  LocalPeer returns the local peer associated with this network

   not overriding function Local_Private_Key
     (Self : Connection) return P2P.Crypto.Private_Key'Class;

   not overriding function Remote_Peer
     (Self : Connection) return P2P.Peers.Id;
   --  RemotePeer returns the peer ID of the remote peer

   not overriding function Remote_Public_Key
     (Self : Connection) return P2P.Crypto.Public_Key'Class;
   --  RemotePublicKey returns the public key of the remote peer

   not overriding procedure Close (Self : in out Connection);

   -- ConnMultiaddrs --

   not overriding function Local_Multiaddr
     (Self :  Connection) return Network.Addresses.Address;
   --  LocalMultiaddr returns the local Multiaddr associated with this
   --  connection

   not overriding function Remote_Multiaddr
     (Self :  Connection) return Network.Addresses.Address;
   --  RemoteMultiaddr returns the remote Multiaddr associated with this
   --  connection

   procedure Initialize
     (Self : out Connection;
      Promise : Network.Connection_Promises.Promise);

private

   type Listener (Parent : access Connection'Class) is
     new Network.Connections.Listener
     and Network.Connection_Promises.Listener with null record;

   overriding procedure On_Resolve
     (Self  : in out Listener;
      Value : Network.Connections.Connection_Access);

   overriding procedure On_Reject
     (Self  : in out Listener;
      Value : League.Strings.Universal_String);

   overriding procedure Closed
     (Self  : in out Listener;
      Error : League.Strings.Universal_String);

   overriding procedure Can_Write (Self : in out Listener);

   overriding procedure Can_Read (Self : in out Listener);

   type Connection is tagged limited record
      Promise    : Network.Connection_Promises.Promise;
      Link       : Network.Connections.Connection_Access;
      Listener   : aliased Connections.Listener (Connection'Unchecked_Access);
      Input      : Ada.Streams.Stream_Element_Array (1 .. 127);
      Input_Last : Ada.Streams.Stream_Element_Count;
   end record;

end P2P.Connections;
