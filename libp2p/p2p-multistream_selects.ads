with Ada.Streams;
with Ada.Tags;
with Ada.Finalization;

with League.Strings;
with League.Strings.Hash;

with Network.Connection_Promises;
with Network.Connections;
with Network.Addresses;
with Network.Generic_Promises;
with Crypto.Pb.Crypto;

package P2P.Multistream_Selects is

   type Context is record
      Connection : Network.Connections.Connection_Access;
      --  Raw connection to be encrypted
      Pubkey     : Crypto.Pb.Crypto.Public_Key;
      --  Local peer public key
      Promise    : not null access Network.Connection_Promises.Controller;
      --  Promise to be resolved when connection ready
   end record;

   type Encryption is limited interface;

   not overriding function Create
     (Parent : not null access Context)
        return Encryption is abstract;

   type Tag_Array is array (Positive range <>) of Ada.Tags.Tag;

   procedure Upgrade_To_Encrypted
     (Pubkey : Crypto.Pb.Crypto.Public_Key;
      Link   : in out Network.Connection_Promises.Promise;
      Tags   : Tag_Array;
      Result : out Network.Connection_Promises.Promise)
        with Pre => not Link.Is_Attached,
            Post => not Result.Is_Attached;
   --  Return promise to be resolved with an encrypted connection upgraded from
   --  Link. Use Pubkey in handshake. Each element of Tags should implement
   --  Encryption interface and has External_Tag corresponding to the protocol
   --  name (e.g. "/plaintext/2.0.0").

private

   type Encryption_Access is access all Encryption'Class;

   type Listener
     (Count  : Natural;
      Length : Ada.Streams.Stream_Element_Count)
   is new Ada.Finalization.Limited_Controlled
     and Network.Connection_Promises.Listener
     and Network.Connections.Listener with
      record
         Promise  : aliased Network.Connection_Promises.Controller;
         Pubkey   : Crypto.Pb.Crypto.Public_Key;
         Link     : Network.Connections.Connection_Access;
         Child    : Encryption_Access;
         Listener : Network.Connections.Listener_Access;
         Error    : League.Strings.Universal_String;
         Remote   : Network.Addresses.Address;
         Index    : Positive;
         Left     : Ada.Streams.Stream_Element_Offset;  --  Bytes to read
         Last     : Ada.Streams.Stream_Element_Offset;  --  Bytes we have
         Tags     : Tag_Array (1 .. Count);
         Buffer   : Ada.Streams.Stream_Element_Array (1 .. Length);
      end record;

   type Listener_Access is access all Listener;

   overriding procedure On_Resolve
     (Self  : in out Listener;
      Value : Network.Connections.Connection_Access);

   overriding procedure On_Reject
     (Self  : in out Listener;
      Error : League.Strings.Universal_String);

   overriding procedure Closed
     (Self  : in out Listener;
      Error : League.Strings.Universal_String);

   overriding procedure Can_Write (Self : in out Listener);

   overriding procedure Can_Read (Self : in out Listener);

end P2P.Multistream_Selects;
