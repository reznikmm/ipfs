with Network.Addresses;
with Network.Connections;

with League.Strings;
with Network.Connection_Promises;

package body P2P.Dialers is

   ----------------
   -- Local_Peer --
   ----------------

   not overriding function Local_Peer (Self : Dialer) return P2P.Peers.Id is
   begin
      return Self.Local_Peer;
   end Local_Peer;

   ---------------
   -- Dial_Peer --
   ---------------

   not overriding procedure Dial_Peer
     (Self   : in out Dialer;
      Peer   : P2P.Peers.Id;
      Result : out P2P.Connections.Connection)
   is
      Error  : League.Strings.Universal_String;
      Remote : Network.Addresses.Address :=
        Network.Addresses.To_Address
          (League.Strings.To_Universal_String ("/ip4/127.0.0.1/tcp/4001"));
      Promise : Network.Connection_Promises.Promise;
   begin
      Self.Manager.Connect
        (Address => Remote,
         Error   => Error,
         Promise  => Promise);

--      Link.Set_Listener (Self.Listener'Unchecked_Access);

      Result.Initialize (Promise);
   end Dial_Peer;

end P2P.Dialers;
