with Ada.Streams;

with League.Text_Codecs;
with League.Characters.Latin;

package body P2P.Connections is

   function To_Message
     (Data : Ada.Streams.Stream_Element_Array)
        return Ada.Streams.Stream_Element_Array;

   overriding procedure Can_Read (Self : in out Listener) is
      use type Ada.Streams.Stream_Element;
      use type Ada.Streams.Stream_Element_Count;
   begin
      if Self.Parent.Input_Last = 0 then
         Self.Parent.Link.Read
           (Self.Parent.Input (1 .. 1), Self.Parent.Input_Last);
         pragma Assert (Self.Parent.Input_Last = 1);
         pragma Assert (Self.Parent.Input (1) < 128);
      else
         Self.Parent.Link.Read
           (Self.Parent.Input (1 .. 1), Self.Parent.Input_Last);
      end if;
   end Can_Read;

   overriding procedure Can_Write (Self : in out Listener) is null;

   overriding procedure Closed
     (Self  : in out Listener;
      Error : League.Strings.Universal_String) is null;

   overriding procedure On_Reject
     (Self  : in out Listener;
      Value : League.Strings.Universal_String) is null;

   overriding procedure On_Resolve
     (Self  : in out Listener;
      Value : Network.Connections.Connection_Access)
   is
      use type Ada.Streams.Stream_Element_Count;
      use type League.Strings.Universal_String;

      function "+" (Text : Wide_Wide_String)
        return League.Strings.Universal_String
          renames League.Strings.To_Universal_String;

      Last : Ada.Streams.Stream_Element_Count;

      UTF_8 : constant League.Text_Codecs.Text_Codec :=
        League.Text_Codecs.Codec (+"utf-8");

      Text : League.Strings.Universal_String :=
        +"/multistream/1.0.0" & League.Characters.Latin.Line_Feed;

      MS : constant Ada.Streams.Stream_Element_Array := To_Message
        (UTF_8.Encode (Text).To_Stream_Element_Array);
   begin
      Self.Parent.Link.Set_Listener (Self'Unchecked_Access);
      Self.Parent.Link.Write (MS, Last);
      pragma Assert (Last = MS'Last);
   end On_Resolve;

   ----------------
   -- Initialize --
   ----------------

   procedure Initialize
     (Self : out Connection;
      Promise : Network.Connection_Promises.Promise) is
   begin
      Self.Promise := Promise;
      Self.Input_Last := 0;
   end Initialize;

   ----------------
   -- To_Message --
   ----------------

   function To_Message
     (Data : Ada.Streams.Stream_Element_Array)
        return Ada.Streams.Stream_Element_Array
   is
      use type Ada.Streams.Stream_Element_Array;
   begin
      pragma Assert (Data'Length < 128);
      return Data'Length & Data;
   end To_Message;

   pragma Warnings (Off);

   ----------------
   -- Local_Peer --
   ----------------

   not overriding function Local_Peer (Self : Connection) return P2P.Peers.Id
   is
   begin
      pragma Compile_Time_Warning (Standard.True, "Local_Peer unimplemented");
      return raise Program_Error with "Unimplemented function Local_Peer";
   end Local_Peer;

   -----------------------
   -- Local_Private_Key --
   -----------------------

   not overriding function Local_Private_Key
     (Self : Connection) return P2P.Crypto.Private_Key'Class
   is
   begin
      pragma Compile_Time_Warning (Standard.True,
         "Local_Private_Key unimplemented");
      return raise Program_Error
          with "Unimplemented function Local_Private_Key";
   end Local_Private_Key;

   -----------------
   -- Remote_Peer --
   -----------------

   not overriding function Remote_Peer (Self : Connection) return P2P.Peers.Id
   is
   begin
      pragma Compile_Time_Warning (Standard.True, "Remote_Peer unimplemented");
      return raise Program_Error with "Unimplemented function Remote_Peer";
   end Remote_Peer;

   -----------------------
   -- Remote_Public_Key --
   -----------------------

   not overriding function Remote_Public_Key
     (Self : Connection) return P2P.Crypto.Public_Key'Class
   is
   begin
      pragma Compile_Time_Warning (Standard.True,
         "Remote_Public_Key unimplemented");
      return raise Program_Error
          with "Unimplemented function Remote_Public_Key";
   end Remote_Public_Key;

   -----------
   -- Close --
   -----------

   not overriding procedure Close (Self : in out Connection) is
   begin
      pragma Compile_Time_Warning (Standard.True, "Close unimplemented");
      raise Program_Error with "Unimplemented procedure Close";
   end Close;

   ---------------------
   -- Local_Multiaddr --
   ---------------------

   not overriding function Local_Multiaddr
     (Self : Connection) return Network.Addresses.Address
   is
   begin
      pragma Compile_Time_Warning (Standard.True,
         "Local_Multiaddr unimplemented");
      return raise Program_Error with "Unimplemented function Local_Multiaddr";
   end Local_Multiaddr;

   ----------------------
   -- Remote_Multiaddr --
   ----------------------

   not overriding function Remote_Multiaddr
     (Self : Connection) return Network.Addresses.Address
   is
   begin
      pragma Compile_Time_Warning (Standard.True,
         "Remote_Multiaddr unimplemented");
      return raise Program_Error
          with "Unimplemented function Remote_Multiaddr";
   end Remote_Multiaddr;

end P2P.Connections;
