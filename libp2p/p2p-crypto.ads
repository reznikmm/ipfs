-- Package crypto implements various cryptographic utilities used by libp2p.
-- This includes a Public and Private key interface and key implementations
-- for supported key algorithms.

with Crypto.Pb.Crypto; use Crypto;

with League.Stream_Element_Vectors;
with League.Strings;

package P2P.Crypto is

   type Key is interface;

   not overriding function Key_Type
     (Self : Key) return Pb.Crypto.Key_Type is abstract;

   type Public_Key is interface and Key;

   not overriding function Verify
     (Self : Public_Key;
      Data : League.Stream_Element_Vectors.Stream_Element_Vector;
      Sign : League.Stream_Element_Vectors.Stream_Element_Vector)
      return Boolean is abstract;

   type Private_Key is interface and Key;

   not overriding procedure Sign
     (Self   : Private_Key;
      Data   : League.Stream_Element_Vectors.Stream_Element_Vector;
      Result : out League.Stream_Element_Vectors.Stream_Element_Vector;
      Errot  : out League.Strings.Universal_String)
   is abstract;

private

end P2P.Crypto;
