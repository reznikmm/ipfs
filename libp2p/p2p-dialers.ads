with P2P.Connections;
with P2P.Peers;

with Network.Managers;

package P2P.Dialers is

   type Dialer is tagged limited private;
   --  Dialer represents a service that can dial out to peers (this is usually
   --  just a Network, but other services may not need the whole stack, and
   --  thus it becomes easier to mock)

   not overriding function Local_Peer
     (Self : Dialer) return P2P.Peers.Id;
   --  LocalPeer returns the local peer associated with this network

   not overriding procedure Dial_Peer
     (Self   : in out Dialer;
      Peer   : P2P.Peers.Id;
      Result : out P2P.Connections.Connection);
   --  Dial_Peer establishes a connection to a given peer

private

   type Dialer is tagged limited record
      Manager    : Network.Managers.Manager;
      Local_Peer : P2P.Peers.Id;
   end record;

end P2P.Dialers;
