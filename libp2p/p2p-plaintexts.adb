with Plaintext.Pb.Plaintext; use Plaintext;
with PB_Support.Memory_Streams;
with Crypto.Pb.Crypto;

with Ada.Wide_Wide_Text_IO;
with League.Base_Codecs;
with GNAT.SHA256;

package body P2P.Plaintexts is

   procedure On_Message (Self : in out Plaintext'Class);
   procedure Send_Handshake (Self : in out Plaintext'Class);
   function To_Length (Length : Ada.Streams.Stream_Element_Count)
     return Ada.Streams.Stream_Element_Array;

   --------------
   -- Can_Read --
   --------------

   overriding procedure Can_Read (Self : in out Plaintext) is
      use type Ada.Streams.Stream_Element;
      use type Ada.Streams.Stream_Element_Count;
      Last   : Ada.Streams.Stream_Element_Count;
   begin
      loop
         if Self.Handshake (Network.Polls.Input) then
            exit;

         elsif Self.Left = 0 then
            declare
               Buffer : Ada.Streams.Stream_Element_Array (1 .. 1);
            begin
               Self.Link.Read (Buffer, Last);

               exit when Last < Buffer'First;

               Self.Input.Append (Buffer);

               if Buffer (1) < 128 then
                  Last := 0;

                  for J in reverse 1 .. Self.Input.Length loop
                     Last := 128 * Last + Ada.Streams.Stream_Element_Count
                       (Self.Input.Element (J) and 127);
                  end loop;

                  Self.Left := Last;
                  Self.Input.Clear;
               end if;
            end;
         else
            declare
               Max : constant Ada.Streams.Stream_Element_Count :=
                 Ada.Streams.Stream_Element_Count'Min
                   (Self.Input.Length + 512, Self.Left);

               Buffer : Ada.Streams.Stream_Element_Array
                 (Self.Input.Length + 1 .. Max);
            begin
               Self.Link.Read (Buffer, Last);

               exit when Last < Buffer'First;

               Self.Input.Append (Buffer (Buffer'First .. Last));

               if Self.Input.Length = Self.Left then
                  Self.On_Message;
               end if;
            end;
         end if;
      end loop;
   end Can_Read;

   ---------------
   -- Can_Write --
   ---------------

   overriding procedure Can_Write (Self : in out Plaintext) is
   begin
      if not Self.Handshake (Network.Polls.Output) then
         Self.Send_Handshake;
      end if;
   end Can_Write;

   ------------
   -- Closed --
   ------------

   overriding procedure Closed
     (Self  : in out Plaintext;
      Error : League.Strings.Universal_String) is
   begin
      Self.Promise.Reject (Error);
   end Closed;

   ------------
   -- Create --
   ------------

   overriding function Create
     (Parent : not null access P2P.Multistream_Selects.Context)
      return Plaintext is
   begin
      return Result : aliased Plaintext (Parent.Connection, Parent.Promise) do
         Result.Handshake := (False, False);
         Result.Left := 0;
         Result.Pubkey := Parent.Pubkey;
         Result.Link.Set_Listener (Result'Unchecked_Access);
      end return;
   end Create;

   ----------------
   -- On_Message --
   ----------------

   procedure On_Message (Self : in out Plaintext'Class) is
      Object : Pb.Plaintext.Exchange;
      Stream : aliased PB_Support.Memory_Streams.Memory_Stream;
   begin
      Stream.Write (Self.Input.To_Stream_Element_Array);
      Pb.Plaintext.Exchange'Read (Stream'Unchecked_Access, Object);
      Self.Handshake (Network.Polls.Input) := True;

      if Self.Handshake = (True, True) then
         Self.Promise.Resolve (Self.Link);
      end if;
   end On_Message;

   --------------------
   -- Send_Handshake --
   --------------------

   procedure Send_Handshake (Self : in out Plaintext'Class) is
      use type Ada.Streams.Stream_Element_Count;

      Last   : Ada.Streams.Stream_Element_Offset;
      Hash   : GNAT.SHA256.Binary_Message_Digest;
      Id     : League.Stream_Element_Vectors.Stream_Element_Vector;
      Object : Pb.Plaintext.Exchange;
   begin

      declare
         Stream : aliased PB_Support.Memory_Streams.Memory_Stream;
      begin
         Object.Pubkey := (True, Self.Pubkey);
         Crypto.Pb.Crypto.Public_Key'Write
           (Stream'Unchecked_Access, Object.Pubkey.Value);

         Hash := GNAT.SHA256.Digest (Stream.Data.To_Stream_Element_Array);
         Id.Append (16#12#);  --  sha2-256
         Id.Append (Hash'Length);  --  length
         Id.Append (Hash);  --  hash

         Object.Id :=
           (Is_Set => True,
            Value  => Id);
      end;

      declare
         Stream : aliased PB_Support.Memory_Streams.Memory_Stream;
      begin
         Pb.Plaintext.Exchange'Write (Stream'Unchecked_Access, Object);

         declare
            use type Ada.Streams.Stream_Element_Array;
            Data : Ada.Streams.Stream_Element_Array :=
              To_Length (Stream.Written) & Stream.Data.To_Stream_Element_Array;
         begin
            Self.Link.Write (Data, Last);
            pragma Assert (Last = Data'Last);

            Self.Handshake (Network.Polls.Output) := True;

            if Self.Handshake = (True, True) then
               Self.Promise.Resolve (Self.Link);
            end if;
         end;
      end;
   end Send_Handshake;

   ---------------
   -- To_Length --
   ---------------

   function To_Length (Length : Ada.Streams.Stream_Element_Count)
     return Ada.Streams.Stream_Element_Array is
      use type Ada.Streams.Stream_Element_Count;
   begin
      if Length < 128 then
         return (1 => Ada.Streams.Stream_Element (Length));
      elsif Length < 128 * 128 then
         return (Ada.Streams.Stream_Element ((Length mod 128) + 128),
                 Ada.Streams.Stream_Element (Length / 128));
      else
         raise Program_Error;
      end if;
   end To_Length;

end P2P.Plaintexts;
