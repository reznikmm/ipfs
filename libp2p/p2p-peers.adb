pragma Ada_2012;
package body P2P.Peers is

   pragma Warnings (Off);

   ------------
   -- Decode --
   ------------

   not overriding procedure Decode
     (Self : out Id; Value : League.Strings.Universal_String)
   is
   begin
      pragma Compile_Time_Warning (Standard.True, "Decode unimplemented");
      raise Program_Error with "Unimplemented procedure Decode";
   end Decode;

   ------------
   -- String --
   ------------

   not overriding function String
     (Self : Id) return League.Strings.Universal_String
   is
   begin
      pragma Compile_Time_Warning (Standard.True, "String unimplemented");
      return raise Program_Error with "Unimplemented function String";
   end String;

end P2P.Peers;
