with Ada.Streams;

with League.Stream_Element_Vectors;
with League.Strings;

with Network.Addresses;
with Network.Connection_Promises;
with Network.Connections;
with Network.Polls;
with Network.Streams;

with P2P.Multistream_Selects;
with Crypto.Pb.Crypto;

package P2P.Plaintexts is

   type Plaintext (<>) is limited
     new P2P.Multistream_Selects.Encryption
       and Network.Connections.Listener
   with private;

private

   type Handshakes is
     array (Network.Polls.Input .. Network.Polls.Output) of Boolean;

   type Plaintext
     (Link    : Network.Connections.Connection_Access;
      Promise : not null access Network.Connection_Promises.Controller)
   is limited new P2P.Multistream_Selects.Encryption
                    and Network.Connections.Listener
   with record
      Handshake : Handshakes;  --  Completed exchanges
      Pubkey    : Crypto.Pb.Crypto.Public_Key;
      Input     : League.Stream_Element_Vectors.Stream_Element_Vector;
      Left      : Ada.Streams.Stream_Element_Count;
   end record;

   for Plaintext'External_Tag use "/plaintext/2.0.0";

   overriding function Create
     (Parent : not null access P2P.Multistream_Selects.Context)
        return Plaintext;

   overriding procedure Closed
     (Self  : in out Plaintext;
      Error : League.Strings.Universal_String);

   overriding procedure Can_Read (Self : in out Plaintext);

   overriding procedure Can_Write (Self : in out Plaintext);

end P2P.Plaintexts;
