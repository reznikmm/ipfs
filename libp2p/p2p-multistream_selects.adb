with Ada.Tags.Generic_Dispatching_Constructor;

with League.Characters.Latin;
with League.Text_Codecs;

package body P2P.Multistream_Selects is

   function "+" (Text : Wide_Wide_String)
     return League.Strings.Universal_String
       renames League.Strings.To_Universal_String;

   function "-" (Tag : Ada.Tags.Tag)
     return League.Strings.Universal_String is
       (League.Strings.From_UTF_8_String (Ada.Tags.External_Tag (Tag)));

   function To_Message
     (Data : Ada.Streams.Stream_Element_Array)
        return Ada.Streams.Stream_Element_Array;

   Name : constant Wide_Wide_String := "/multistream/1.0.0";

   UTF_8 : constant League.Text_Codecs.Text_Codec :=
     League.Text_Codecs.Codec (+"utf-8");

   function Bin (Value : League.Strings.Universal_String)
     return Ada.Streams.Stream_Element_Array is
       (UTF_8.Encode (Value).To_Stream_Element_Array);

   procedure On_Message
     (Self : in out Listener'Class;
      Done : in out Boolean);
   --  New message in Self.Buffer (1 .. Self.Last);

   function Constructor is new Ada.Tags.Generic_Dispatching_Constructor
     (T           => Encryption,
      Parameters  => Context,
      Constructor => Create);

   --------------
   -- Can_Read --
   --------------

   overriding procedure Can_Read (Self : in out Listener) is
      use type Ada.Streams.Stream_Element_Count;

      Done   : Boolean := False;
      Last   : Ada.Streams.Stream_Element_Count;
      First  : Ada.Streams.Stream_Element_Count;
      Buffer : Ada.Streams.Stream_Element_Array (1 .. 1);
   begin
      --  Try to read a whole message
      loop
         if Self.Left = 0 then  --  Read length
            Self.Link.Read (Buffer, Last);
            pragma Assert (Last = 1);
            pragma Assert (Buffer (1) in 1 .. Buffer'Length);
            Self.Left := Ada.Streams.Stream_Element_Count (Buffer (1));
            Self.Last := 0;
         else
            First := Self.Last + 1;
            Self.Link.Read (Self.Buffer (First .. Self.Left), Last);
            Self.Last := Last;

            if Self.Last = Self.Left then
               Self.On_Message (Done);
            end if;

            exit when Done or Last < First;

            Self.Left := 0;
         end if;
      end loop;
   end Can_Read;

   ---------------
   -- Can_Write --
   ---------------

   overriding procedure Can_Write (Self : in out Listener) is
      use type League.Strings.Universal_String;
      use type Ada.Streams.Stream_Element_Array;
      use type Ada.Streams.Stream_Element_Count;

      Header : constant League.Strings.Universal_String :=
        +Name & League.Characters.Latin.Line_Feed;
      First  : constant League.Strings.Universal_String :=
        -Self.Tags (1) & League.Characters.Latin.Line_Feed;
      M1     :  constant Ada.Streams.Stream_Element_Array :=
        To_Message (UTF_8.Encode (Header).To_Stream_Element_Array);
      M2     :  constant Ada.Streams.Stream_Element_Array :=
        To_Message (UTF_8.Encode (First).To_Stream_Element_Array);
      Data   : constant Ada.Streams.Stream_Element_Array := M1 & M2;
      Last   : Ada.Streams.Stream_Element_Count;
   begin
      Self.Link.Write (Data, Last);
      pragma Assert (Last = Data'Last);
   end Can_Write;

   ------------
   -- Closed --
   ------------

   overriding procedure Closed
     (Self  : in out Listener;
      Error : League.Strings.Universal_String)
   is
   begin
      Self.Promise.Reject (Error);
   end Closed;

   ----------------
   -- On_Message --
   ----------------

   procedure On_Message
     (Self : in out Listener'Class;
      Done : in out Boolean)
   is
      use type Ada.Streams.Stream_Element;
      use type Ada.Streams.Stream_Element_Count;
      use type Ada.Streams.Stream_Element_Array;

      Data : Ada.Streams.Stream_Element_Array renames
        Self.Buffer (1 .. Self.Last - 1);
   begin
      pragma Assert (Self.Buffer (Self.Last) = 16#0a#);

      if Data = Bin (+Name) then  --  /multistream/1.0.0
         null;  --  Change state???
      elsif Data'Length = 2 and then Data = Bin (+"na") then --  na
         raise Program_Error;
      elsif Data = Bin (-Self.Tags (Self.Index)) then  --  accept

         declare
            Object : aliased Context :=
              (Connection => Self.Link,
               Pubkey     => Self.Pubkey,
               Promise    => Self.Promise'Unchecked_Access);
         begin
            Self.Link.Set_Listener (null);

            Self.Child := new Encryption'Class
              '(Constructor (Self.Tags (Self.Index), Object'Access));

            Done := True;
         end;

      elsif Data'Length = 2 and then Data = Bin (+"ls") then --  ls
         raise Program_Error;
      else
         raise Program_Error;
      end if;
   end On_Message;

   ---------------
   -- On_Reject --
   ---------------

   overriding procedure On_Reject
     (Self  : in out Listener;
      Error : League.Strings.Universal_String) is
   begin
      Self.Promise.Reject (Error);
   end On_Reject;

   ----------------
   -- On_Resolve --
   ----------------

   overriding procedure On_Resolve
     (Self  : in out Listener;
      Value : Network.Connections.Connection_Access)
   is
   begin
      Self.Link := Value;
      Value.Set_Listener (Self'Unchecked_Access);
      Self.Index := 1;
   end On_Resolve;

   ----------------
   -- To_Message --
   ----------------

   function To_Message
     (Data : Ada.Streams.Stream_Element_Array)
        return Ada.Streams.Stream_Element_Array
   is
      use type Ada.Streams.Stream_Element_Array;
   begin
      pragma Assert (Data'Length < 128);
      return Data'Length & Data;
   end To_Message;

   --------------------------
   -- Upgrade_To_Encrypted --
   --------------------------

   procedure Upgrade_To_Encrypted
     (Pubkey : Crypto.Pb.Crypto.Public_Key;
      Link   : in out Network.Connection_Promises.Promise;
      Tags   : Tag_Array;
      Result : out Network.Connection_Promises.Promise)
   is
      use type Ada.Streams.Stream_Element_Count;

      Length : Ada.Streams.Stream_Element_Count := Name'Length;
      Object : Listener_Access;
   begin
      for Tag of Tags loop
         Length := Ada.Streams.Stream_Element_Count'Max
           (Length,
            Ada.Streams.Stream_Element_Count
              (Ada.Tags.External_Tag (Tag)'Length));
      end loop;

      Length := Length + 2;  --  LF & Size
      Object := new Listener (Tags'Length, Length);
      Object.Tags := Tags;
      Object.Last := 0;
      Object.Left := 0;
      Object.Pubkey := Pubkey;
      Link.Add_Listener (Object.all'Access);
      Result := Object.Promise.To_Promise;
   end Upgrade_To_Encrypted;

end P2P.Multistream_Selects;
