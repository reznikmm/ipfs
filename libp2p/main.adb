with Ada.Streams;
with Ada.Wide_Wide_Text_IO;

with League.Strings;
with League.String_Vectors;
with League.Base_Codecs;

with P2P.Dialers;
with P2P.Peers;
with P2P.Multistream_Selects;
with P2P.Plaintexts;
with Crypto.Pb.Crypto;

with Network.Addresses;
with Network.Managers;
with Network.Connections;
with Network.Managers.TCP_V4;
with League.Stream_Element_Vectors;
with Network.Connection_Promises;

procedure Main is

   function "+" (Text : Wide_Wide_String)
     return League.Strings.Universal_String
       renames League.Strings.To_Universal_String;

   package Listeners is

      type Listener is new Network.Connection_Promises.Listener
        and Network.Connections.Listener with
      record
         Link   : Network.Connections.Connection_Access;
         Closed : Boolean := False;
      end record;

      overriding procedure On_Resolve
        (Self  : in out Listener;
         Value : Network.Connections.Connection_Access);

      overriding procedure On_Reject
        (Self  : in out Listener;
         Value : League.Strings.Universal_String);

      overriding procedure Closed
        (Self  : in out Listener;
         Error : League.Strings.Universal_String);

      overriding procedure Can_Read (Self  : in out Listener);
      overriding procedure Can_Write (Self  : in out Listener);

   end Listeners;

   package body Listeners is

      overriding procedure Can_Read (Self  : in out Listener) is
         use type Ada.Streams.Stream_Element_Count;

         Data : Ada.Streams.Stream_Element_Array (1 .. 256);
         Last : Ada.Streams.Stream_Element_Count;
      begin
         loop
            Self.Link.Read (Data, Last);
            exit when Last = 0;
         end loop;
      end Can_Read;

      overriding procedure Can_Write (Self  : in out Listener) is
         Last : Ada.Streams.Stream_Element_Count;
         Data : Ada.Streams.Stream_Element_Array (1 .. 24) :=
           (19,
            16#2F#, 16#6D#, 16#75#, 16#6C#, 16#74#, 16#69#, 16#73#, 16#74#,
            16#72#, 16#65#, 16#61#, 16#6D#, 16#2F#, 16#31#, 16#2E#, 16#30#,
            16#2E#, 16#30#, 16#0A#,
            3,
            16#6c#, 16#73#, 16#0a#);
      begin
         Self.Link.Write (Data, Last);
      end Can_Write;

      overriding procedure Closed
        (Self  : in out Listener;
         Error : League.Strings.Universal_String) is
      begin
         Ada.Wide_Wide_Text_IO.Put_Line (Error.To_Wide_Wide_String);
         Self.Closed := True;
      end Closed;

      overriding procedure On_Reject
        (Self  : in out Listener;
         Value : League.Strings.Universal_String) renames Closed;

      overriding procedure On_Resolve
        (Self  : in out Listener;
         Value : Network.Connections.Connection_Access)
      is
      begin
         Self.Link := Value;
         Self.Link.Set_Listener (Self'Unchecked_Access);
      end On_Resolve;

   end Listeners;

   procedure Read_Key (Pubkey : out Crypto.Pb.Crypto.Public_Key) is
      Input  : Ada.Wide_Wide_Text_IO.File_Type;
      Text   : League.Strings.Universal_String;
      Key    : League.Stream_Element_Vectors.Stream_Element_Vector;

   begin
      Ada.Wide_Wide_Text_IO.Open
        (Input, Ada.Wide_Wide_Text_IO.In_File, "/tmp/key.pem");

      while not Ada.Wide_Wide_Text_IO.End_Of_File (Input) loop
         declare
            Line : Wide_Wide_String :=
              Ada.Wide_Wide_Text_IO.Get_Line (Input);
         begin
            if Line'Length > 0 and then Line (Line'First) /= '-' then
               Text.Append (Line);
            end if;
         end;
      end loop;

      Ada.Wide_Wide_Text_IO.Close (Input);
      Key := League.Base_Codecs.From_Base_64 (Text);

      Pubkey := (PB_Type => Crypto.Pb.Crypto.RSA,
                 Data    => Key);
   end Read_Key;


   Promise : Network.Connection_Promises.Promise;
   Pubkey  : Crypto.Pb.Crypto.Public_Key;
   Error   : League.Strings.Universal_String;
   Manager : Network.Managers.Manager;
   Id      : P2P.Peers.Id;
   Lnr     : aliased Listeners.Listener;
begin
   Manager.Initialize;
   Network.Managers.TCP_V4.Register (Manager);
   Read_Key (Pubkey);

   Manager.Connect
     (Address => Network.Addresses.To_Address
        (+"/ip4/127.0.0.1/tcp/4001"),
      Error   => Error,
      Promise => Promise);

   if not Error.Is_Empty then
      Ada.Wide_Wide_Text_IO.Put_Line
        ("Connect error:" & Error.To_Wide_Wide_String);
      return;
   end if;

   declare
      Next  : Network.Connection_Promises.Promise;
      Tags  : P2P.Multistream_Selects.Tag_Array (1 .. 1);
   begin
      Tags (1) := P2P.Plaintexts.Plaintext'Tag;
      P2P.Multistream_Selects.Upgrade_To_Encrypted
        (Pubkey, Promise, Tags, Next);
      Next.Add_Listener (Lnr'Unchecked_Access);
   end;

   while not Lnr.Closed loop
      Manager.Wait (1.0);
   end loop;
end Main;
