with League.Strings;

package P2P.Peers is

   type Id is tagged private;

   not overriding procedure Decode
     (Self  : out Id;
      Value : League.Strings.Universal_String);

   not overriding function String
     (Self : Id) return League.Strings.Universal_String;

private
   type Id is tagged record
      Value : League.Strings.Universal_String;
   end record;

end P2P.Peers;
